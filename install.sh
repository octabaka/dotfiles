#!/usr/bin/env bash

set -euo pipefail

readonly mes_fichiers=(
  ".bash_profile" ".bashrc" ".inputrc"
  ".config/lf/lfrc" ".config/lf/pv.sh"
  ".config/nvim" ".config/clangd"
  ".tmux.conf"
)

main() {
  check_os
}

install_paquets=false

check_os() {
  if command -v "apt" &> /dev/null; then
    echo "Debian détecté"
    demande_install
    if $install_paquets; then
      sudo apt install bash neovim tmux fzf grc ncdu fd-find
    fi
    copy_files
  elif command -v "pacman" &> /dev/null; then
    echo "Arch Linux détecté"
    demande_install
    if $install_paquets; then
      sudo pacman -S --needed bash neovim tmux fzf grc ncdu fd ffmpeg flac arduino-cli avr-gcc automake avrdude avr-libc bash-completion bash-language-server bzip2 ca-certificates clang cmake curl dart-sass dfu-programmer dfu-util faac gcc gcc-libs git gopls handlr gzip htop hugo imagemagick inotify-tools jq just kconfig lame lf llvm lua lua-language-server make mediainfo ncurses ninja nmap nodejs npm openssl opus p7zip pulseaudio pulseaudio-alsa python qmk ripgrep rsync sassc sqlite tmux tree typescript typescript-language-server unzip watchexec x264 x265 yarn yaml-cpp yaml-language-server yt-dlp zig zls net-tools bison gperf ninja ccache dfu-util libusb
    fi
    copy_files
  elif command -v "opkg" &> /dev/null; then
    echo "OpenWrt détecté"
    copy_files
  elif command -v "ipkg" &> /dev/null; then
    echo "NAS détecté"
    copy_files
  else
    echo "Not supported"
    exit 1
  fi  
}
demande_install() {
  while true; do
    read -r -p "installer les paquets (y/N) " yn
    case $yn in 
    [yYoO]) 
      install_paquets=true
   	  break
    ;;
   	*)
      install_paquets=false
   	  break
    ;;
    esac
  done
}

copy_files() {
  for mon_fichier in "${mes_fichiers[@]}"; do
    check_si_existe_dotfiles "$mon_fichier"
    check_si_les_liens_existent "$mon_fichier"
    ln -s "$PWD/$mon_fichier" "$HOME/$mon_fichier"
  done
}

check_si_les_liens_existent() {
  local mon_fichier="$HOME/$1"
  # check le type
  if [[ -L $mon_fichier ]]; then
    # echo "$mon_fichier est un lien symbolique"
    demande_efface "$mon_fichier"
  elif [[ -f $mon_fichier ]]; then
    # echo "$mon_fichier est un fichier"
    demande_efface "$mon_fichier"
  elif [[ -d $mon_fichier ]]; then
    # echo "$mon_fichier est un dossier"
    demande_efface "$mon_fichier"
  else
    mkdir -p -v "$(dirname "$mon_fichier")"
  fi
}

demande_efface() {
  local mon_fichier="$1"
  while true; do
    read -r -p "$mon_fichier existe déjà, l'effacer ? (Y/n) " yn
    case $yn in 
    [nN]) 
      exit 0
   	  break
    ;;
   	*)
      rm -r "$mon_fichier"
   	  break
    ;;
    esac
  done
}

check_si_existe_dotfiles() {
  local mon_fichier="$PWD/$1"
  if ! [[ -f $mon_fichier || -d $mon_fichier ]] ; then
    echo "$mon_fichier n'existe pas dans $PWD"
    exit 1
  fi
}

main "${@}"

echo "All OK !" 1>&2
exit 0
